# Web Crawler App for Monzo

### Running The Web Crawler
* Java Version openjdk64-11.0.8
* `java -jar Web-Crawler-1.0-SNAPSHOT.jar https://monzo.com`

### Consideratioins
* Pages can link to themselves.
* Links that don't work correctly are still tracked
* Redirects are followed and recorded as the redirect unless it doesn't work. In the case that it doesn't work the original url is tracked.

### Changes
* Http calls were the slowest process. Redirects where omitted for now.