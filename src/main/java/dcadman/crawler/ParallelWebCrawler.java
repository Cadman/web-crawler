package dcadman.crawler;

import dcadman.crawler.web.Domain;
import dcadman.crawler.web.LinkFetcher;
import dcadman.crawler.web.PageToLinkData;
import dcadman.crawler.web.SubDomain;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.*;

public class ParallelWebCrawler extends WebCrawler {

    private final int NUMBER_OF_THREADS = 200;
    private final int GRAPH_INITIAL_CAPACITY = 1000;
    private final int QUEUE_INITIAL_CAPACITY = 300;

    private final ExecutorService executor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
    CompletionService<PageToLinkData> completionService = new ExecutorCompletionService<>(executor);

    private LinkFetcher linkFetcher = new LinkFetcher();

    public ParallelWebCrawler(LinkFetcher linkFetcher) {
        this.linkFetcher = linkFetcher;
    }

    public ParallelWebCrawler() {
    }

    @Override
    public Map<String, Set<String>> crawl(Domain startingDomain) {
        Map<String, Set<String>> webSiteGraph = new ConcurrentHashMap<>(GRAPH_INITIAL_CAPACITY);
        Set<String> queueOfLinks = ConcurrentHashMap.newKeySet(QUEUE_INITIAL_CAPACITY);

        queueOfLinks.add(startingDomain.getUrl());
        while(!queueOfLinks.isEmpty()) {
            queueOfLinks.forEach(link -> calculate(new SubDomain(startingDomain, link)));
            queueOfLinks = waitForFuturesAndReturnResults(startingDomain, webSiteGraph, queueOfLinks.size());
        }
        return webSiteGraph;
    }

    private Set<String> waitForFuturesAndReturnResults(Domain startingDomain, Map<String, Set<String>> webSiteGraph, int futuresToWaitFor) {
        Set<String> newLinks = ConcurrentHashMap.newKeySet(QUEUE_INITIAL_CAPACITY);
        while (futuresToWaitFor > 0) {
            try {
                Future<PageToLinkData> result = completionService.take();

                webSiteGraph.put(result.get().getPageAddress(), result.get().getLinks());
                List<String> newPages = getNewPages(startingDomain, webSiteGraph, result.get().getPageAddress());
                newLinks.addAll(newPages);
            }
            catch(Exception e) {
                System.err.println("Error waiting for Future");
            } finally {
                futuresToWaitFor--;
            }
        }
        return newLinks;
    }

    public Future<PageToLinkData> calculate(SubDomain subDomain) {
        return completionService.submit(() -> new PageToLinkData(subDomain.getSubDomainUrl(), linkFetcher.getLinkedPages(subDomain)));
    }
}
