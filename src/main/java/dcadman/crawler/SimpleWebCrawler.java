package dcadman.crawler;

import dcadman.crawler.web.Domain;
import dcadman.crawler.web.LinkFetcher;
import dcadman.crawler.web.SubDomain;

import java.util.*;

public class SimpleWebCrawler extends WebCrawler {

    private final Queue<String> links = new LinkedList<>();
    private final LinkFetcher linkFetcher;

    public SimpleWebCrawler(LinkFetcher linkFetcher) {
        this.linkFetcher = linkFetcher;
    }

    public SimpleWebCrawler() {
        this.linkFetcher = new LinkFetcher();
    }

    @Override
    public Map<String, Set<String>> crawl(Domain startingDomain) {
        Map<String, Set<String>> webSiteGraph = new HashMap<>(3000);
        links.add(startingDomain.getUrl());

        while(!links.isEmpty()) {
            SubDomain subDomain = new SubDomain(startingDomain, links.remove());
            webSiteGraph.put(subDomain.getSubDomainUrl(), linkFetcher.getLinkedPages(subDomain));
            links.addAll(getNewPages(startingDomain, webSiteGraph, subDomain.getSubDomainUrl()));
        }

        return webSiteGraph;
    }

}
