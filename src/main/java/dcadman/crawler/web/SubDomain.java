package dcadman.crawler.web;

public class SubDomain extends Domain {

    private final String subDomainUrl;

    public SubDomain(Domain baseDomain, String subDomainUrl) {
        super(baseDomain.getUrl());
        this.subDomainUrl = subDomainUrl;
    }

    public String getSubDomainUrl() {
        return super.getSubDomainUrl(this.subDomainUrl);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        SubDomain subDomain = (SubDomain) o;

        return subDomainUrl != null ? subDomainUrl.equals(subDomain.subDomainUrl) : subDomain.subDomainUrl == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (subDomainUrl != null ? subDomainUrl.hashCode() : 0);
        return result;
    }
}
