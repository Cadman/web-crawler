package dcadman.crawler.web;

import java.util.Set;

public class PageToLinkData {

    private final String pageAddress;
    private final Set<String> links;

    public PageToLinkData(String pageAddress, Set<String> links) {
        this.pageAddress = pageAddress;
        this.links = links;
    }

    public String getPageAddress() {
        return pageAddress;
    }

    public Set<String> getLinks() {
        return links;
    }
}
