package dcadman.crawler.web;

import org.jsoup.internal.StringUtil;

public class Domain {

    private final String baseDomain;

    public Domain(String baseDomain) {
        this.baseDomain = baseDomain;
    }

    public String getUrl() {
        return baseDomain;
    }

    public boolean checkIfInDomain(String subDomain) {
        return subDomain.startsWith(baseDomain) || subDomain.startsWith("/");
    }

    public String getSubDomainUrl(String subDomain) {
        if (subDomain.startsWith("/")) {
            if (!baseDomain.endsWith("/")) return baseDomain.concat(subDomain);
            return baseDomain.substring(0, baseDomain.length() - 1).concat(subDomain);
        }
        return subDomain;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Domain domain = (Domain) o;

        return baseDomain != null ? baseDomain.equals(domain.baseDomain) : domain.baseDomain == null;
    }

    @Override
    public int hashCode() {
        return baseDomain != null ? baseDomain.hashCode() : 0;
    }
}
