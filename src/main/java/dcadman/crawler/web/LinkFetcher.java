package dcadman.crawler.web;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Collections.emptySet;

public class LinkFetcher {

    public Set<String> getLinkedPages(SubDomain subDomain) {

        Document doc;
        try {
            doc = Jsoup.connect(subDomain.getSubDomainUrl())
                    .userAgent("Mozilla")
                    .timeout(3000)
                    .get();
        } catch (IOException e) {
            return emptySet();
        }

        return doc.select("a[href]").parallelStream()
                .map(x -> x.attr("href"))
                .filter(this::isNotExceptionCase)
                .map(subDomain::getSubDomainUrl)
                // removed for performance improvements .map(this::replaceWithRedirectIfRedirected)
                .collect(Collectors.toSet());
    }

    private boolean isNotExceptionCase(String url) {
        return url.startsWith("https://") || url.startsWith("/") || url.startsWith("http://");
    }

    private String replaceWithRedirectIfRedirected(String url) {
        try {
            return Jsoup.connect(url)
                    .userAgent("Mozilla")
                    .timeout(3000)
                    .followRedirects(true)
                    .execute().url().toString();
        } catch (MalformedURLException e) {
            System.out.println(url);
            return url;
        } catch (IOException e) {
            return url;
        }
    }

}
