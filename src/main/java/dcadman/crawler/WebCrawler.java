package dcadman.crawler;

import dcadman.crawler.web.Domain;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class WebCrawler {
    abstract Map<String, Set<String>> crawl(Domain startingDomain);

    List<String> getNewPages(Domain domain,
                             Map<String, Set<String>> webSiteGraph,
                             String next) {
        return webSiteGraph.get(next).parallelStream()
                .filter(domain::checkIfInDomain)
                .filter(x -> !webSiteGraph.containsKey(x))
                .collect(Collectors.toList());
    }
}
