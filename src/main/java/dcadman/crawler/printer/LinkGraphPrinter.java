package dcadman.crawler.printer;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static java.lang.String.format;

public class LinkGraphPrinter {

    public static void printLinkGraph(Map<String, Set<String>> links) {
        links.entrySet().stream()
                .map(x -> prettyPageLinks(x.getKey(), x.getValue()))
                .forEach(System.out::print);
    }

    private static String prettyPageLinks(String key, Set<String> links) {
        String page = format("\nWeb Page URL: %s\n", key).concat("\t\t Links: \n");
        for (String link : links) {
            String s = printLink(link);
            page = page.concat(s);
        }
        return page;
    }

    private static String printLink(String link) {
        return String.format("\t\t %s\n", link);
    }

}
