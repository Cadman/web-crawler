import dcadman.crawler.ParallelWebCrawler;
import dcadman.crawler.web.Domain;

import java.util.Map;
import java.util.Set;

import static dcadman.crawler.printer.LinkGraphPrinter.printLinkGraph;

public class Main {

    public static void main(String[] args) {
        if (args.length <= 0) {
            System.err.println("Please provide a domain");
            System.exit(1);
        }
        Map<String, Set<String>> crawl = new ParallelWebCrawler().crawl(new Domain(args[0]));
        System.out.println("Finished Mapping Website");
        printLinkGraph(crawl);
        System.exit(0);
    }

}
