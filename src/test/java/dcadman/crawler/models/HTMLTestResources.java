package dcadman.crawler.models;

public class HTMLTestResources {

    public static final String HTML_SINGLE_HREF =
            "<!DOCTYPE html>\n" +
            "<html lang=\"en\">\n" +
            "<head>\n" +
            "    <meta charset=\"UTF-8\">\n" +
            "    <title>Test HTML</title>\n" +
            "</head>\n" +
            "<body>\n" +
            "    <a href=\"http://localhost:8090/test\"></a>\n" +
            "</body>\n" +
            "</html>";

    public static final String HTML_MULTIPLE_HREFS =
            "<!DOCTYPE html>\n" +
                    "<html lang=\"en\">\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <title>Test HTML</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "    <a href=\"http://localhost:8090/starters\"></a>\n" +
                    "    <a href=\"http://localhost:8090/desert\"></a>\n" +
                    "</body>\n" +
                    "</html>";

    public static final String HTML_NO_HREFS =
            "<!DOCTYPE html>\n" +
                    "<html lang=\"en\">\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <title>Test HTML</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "</body>\n" +
                    "</html>";

    public static final String HTML_SUBDOMAIN_HREF =
            "<!DOCTYPE html>\n" +
                    "<html lang=\"en\">\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <title>Test HTML</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "    <a href=\"/test\"></a>\n" +
                    "</body>\n" +
                    "</html>";

    public static final String HTML_UNSAFE_HREFS =
            "<!DOCTYPE html>\n" +
                    "<html lang=\"en\">\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <title>Test HTML</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "    <a href=\"#/test\"></a>\n" +
                    "    <a href=\"%test\"></a>\n" +
                    "    <a href=\"test:\"></a>\n" +
                    "</body>\n" +
                    "</html>";
}
