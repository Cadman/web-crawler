package dcadman.crawler;

import dcadman.crawler.web.Domain;
import dcadman.crawler.web.LinkFetcher;
import dcadman.crawler.web.SubDomain;
import org.hamcrest.Matchers;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static java.util.Collections.emptySet;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

class SimpleWebCrawlerTest {

    LinkFetcher linkFetcher = mock(LinkFetcher.class);
    SimpleWebCrawler simpleWebCrawler = new SimpleWebCrawler(linkFetcher);

    private final static String DOMAIN_HOME = "https://test";
    private final static String LINKED_SUBDOMAIN = "https://test/linked";
    private final static String EXTERNAl_LINK = "https://different-domain";

    private final static Domain DOMAIN = new Domain(DOMAIN_HOME);

    @Test
    public void pageWithNoLinkedPagesIsReturned() {
        when(linkFetcher.getLinkedPages(withSubDomain(DOMAIN_HOME))).thenReturn(emptySet());
        
        assertThat(simpleWebCrawler.crawl(DOMAIN), hasEntry(DOMAIN_HOME,emptySet()));
    }

    @Test
    public void aLinkedPageIsAlsoCrawled() {
        setUpTestSiteWithSingleSubDomainLinked();

        assertThat(simpleWebCrawler.crawl(DOMAIN),
                allOf(hasEntry(DOMAIN_HOME, Set.of(LINKED_SUBDOMAIN)),
                        Matchers.<String, Set<String>>hasEntry(LINKED_SUBDOMAIN, emptySet())));
    }

    @Test
    public void crawlerDoesNotGetCaughtInALoopOfLinks() {
        setUpTestSiteWithLoop();

        simpleWebCrawler.crawl(DOMAIN);
        verify(linkFetcher, times(1)).getLinkedPages(withSubDomain(DOMAIN_HOME));
    }

    @Test
    public void pagesShouldRecordThemselvesIfTheyLinkToThemselves() {
        setUpTestSiteWithSelfLoop();
        assertThat(simpleWebCrawler.crawl(DOMAIN), hasEntry(DOMAIN_HOME, Set.of(DOMAIN_HOME)));
    }

    @Test
    public void externalPagesShouldNotBeNodesInTheGraph() {
        setUpTestSiteWithExternalDomainLink();
        assertThat(simpleWebCrawler.crawl(DOMAIN), not(hasKey(EXTERNAl_LINK)));
    }

    @Test
    public void externallyLinkedPagesShouldBeReferencedByGraph() {
        setUpTestSiteWithExternalDomainLink();
        assertThat(simpleWebCrawler.crawl(DOMAIN), hasEntry(DOMAIN_HOME,Set.of(EXTERNAl_LINK)));
    }


    private void setUpTestSiteWithSingleSubDomainLinked() {
        when(linkFetcher.getLinkedPages(withSubDomain(DOMAIN_HOME))).thenReturn(Set.of(LINKED_SUBDOMAIN));
        when(linkFetcher.getLinkedPages(withSubDomain(LINKED_SUBDOMAIN))).thenReturn(emptySet());
    }

    @NotNull
    private SubDomain withSubDomain(String domainHome) {
        return new SubDomain(DOMAIN, domainHome);
    }

    private void setUpTestSiteWithLoop() {
        when(linkFetcher.getLinkedPages(withSubDomain(DOMAIN_HOME))).thenReturn(Set.of(LINKED_SUBDOMAIN));
        when(linkFetcher.getLinkedPages(withSubDomain(LINKED_SUBDOMAIN))).thenReturn(Set.of(DOMAIN_HOME));
    }

    private void setUpTestSiteWithSelfLoop() {
        when(linkFetcher.getLinkedPages(withSubDomain(DOMAIN_HOME))).thenReturn(Set.of(DOMAIN_HOME));
    }

    private void setUpTestSiteWithExternalDomainLink() {
        when(linkFetcher.getLinkedPages(withSubDomain(DOMAIN_HOME))).thenReturn(Set.of(EXTERNAl_LINK));
    }

}