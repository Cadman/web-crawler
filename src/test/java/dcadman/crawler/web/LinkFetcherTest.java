package dcadman.crawler.web;

import com.github.tomakehurst.wiremock.WireMockServer;
import dcadman.crawler.models.HTMLTestResources;
import org.hamcrest.Matcher;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.core.IsEqual.equalTo;

class LinkFetcherTest {

    LinkFetcher linkFetcher = new LinkFetcher();
    public static WireMockServer wireMockServer;

    private final static Domain DOMAIN = new Domain("http://localhost:8090/");

    @Test
    public void gracefullyHandleFailureOnBrokenLink() {
        assertThat(linkFetcher.getLinkedPages(getSubDomain("http://localhost:8090/")),empty());
    }

    @Test
    public void gracefullyReturnsEmptyOnZeroLinksInResponse() {
        setUpHTMLWithNoLinksOn();
        assertThat(linkFetcher.getLinkedPages(getSubDomain("http://localhost:8090/")),empty());
    }

    @Test
    public void retrievesLinkFromHTML() {
        setUpHTMLWithSingleLinkOn("/");
        assertThat(linkFetcher.getLinkedPages(getSubDomain("http://localhost:8090/")),
                containsLinkToPage("http://localhost:8090/test"));
    }

    @Test
    public void retrievesLinksFromHTML() {
        setUpHTMLWithMultipleLinksOn();
        assertThat(linkFetcher.getLinkedPages(getSubDomain("http://localhost:8090/menu")),
                containsLinksToPages("http://localhost:8090/starters", "http://localhost:8090/desert"));
    }

    //@Test
    //Removed now for performance reasons
    public void fetcherChecksForRedirectsOnRetrievedLinks() {
        setUpHTMLWithSingleLinkOn("/mains");
        setUpHTMLWithSingleLinkOn("/redirect");

        wireMockServer.stubFor(get("/test")
                .willReturn(temporaryRedirect("/redirect")));

        assertThat(linkFetcher.getLinkedPages(getSubDomain("http://localhost:8090/mains")),
                containsLinkToPage("http://localhost:8090/redirect"));
    }

    @Test
    public void subDomainsLinkedAreReturnedWithFullDomainUrl() {
        setUpHTMLWithSubDomainLink();
        assertThat(linkFetcher.getLinkedPages(getSubDomain("http://localhost:8090/")),
                containsLinkToPage("http://localhost:8090/test"));
    }

    @Test
    public void ignoreLinksThatAreNotHTTP() {
        setUpHTMLWithUnsafeLinks();
        assertThat(linkFetcher.getLinkedPages(getSubDomain("http://localhost:8090/")),empty());
    }


    @BeforeAll
    static void beforeAll() {
        wireMockServer = new WireMockServer(wireMockConfig().port(8090));
        wireMockServer.start();
    }

    @BeforeEach
    void setUp() {
        wireMockServer.resetAll();
    }

    @AfterAll
    static void afterAll() {
        wireMockServer.stop();
    }

    private SubDomain getSubDomain(String url) {
        return new SubDomain(DOMAIN, url);
    }

    @NotNull
    private Matcher<Iterable<? extends String>> containsLinksToPages(String page1, String page2) {
        return containsInAnyOrder(equalTo(page1), equalTo(page2));
    }

    @NotNull
    private Matcher<Iterable<? extends String>> containsLinkToPage(String page) {
        return containsInAnyOrder(equalTo(page));
    }

    private void setUpHTMLWithSingleLinkOn(String s) {
        wireMockServer.stubFor(get(s)
                .willReturn(ok()
                        .withHeader("Content-Type", "text/html")
                        .withBody(dcadman.crawler.models.HTMLTestResources.HTML_SINGLE_HREF)));
    }

    private void setUpHTMLWithMultipleLinksOn() {
        wireMockServer.stubFor(get("/menu")
                .willReturn(ok()
                        .withHeader("Content-Type", "text/html")
                        .withBody(dcadman.crawler.models.HTMLTestResources.HTML_MULTIPLE_HREFS)));
    }

    private void setUpHTMLWithNoLinksOn() {
        wireMockServer.stubFor(get("/")
                .willReturn(ok()
                        .withHeader("Content-Type", "text/html")
                        .withBody(HTMLTestResources.HTML_NO_HREFS)));
    }

    private void setUpHTMLWithSubDomainLink() {
        wireMockServer.stubFor(get("/")
                .willReturn(ok()
                        .withHeader("Content-Type", "text/html")
                        .withBody(HTMLTestResources.HTML_SUBDOMAIN_HREF)));
    }

    private void setUpHTMLWithUnsafeLinks() {
        wireMockServer.stubFor(get("/")
                .willReturn(ok()
                        .withHeader("Content-Type", "text/html")
                        .withBody(HTMLTestResources.HTML_UNSAFE_HREFS)));
    }


}