package dcadman.crawler.web;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.jupiter.api.Assertions.*;

class DomainTest {

    Domain testDomain = new Domain("https://testdomain");


    @Test
    public void urlsWithoutBaseDomainArePartOfTheDomain(){
        assertTrue(testDomain.checkIfInDomain("/subdomain/test"));
    }

    @Test
    public void domainsNotInTheDomainAreNotPartOfIt(){
        assertFalse(testDomain.checkIfInDomain("https://anothertestdomain/"));
    }

    @Test
    public void subDomainsCorrectAppearAsIfInDomain(){
        assertTrue(testDomain.checkIfInDomain("https://testdomain/subdomain/test"));
    }

    @Test
    public void urlsWithoutSubDomainForAFullUrl() {
        assertThat(testDomain.getSubDomainUrl("/subdomain/test"), equalTo("https://testdomain/subdomain/test"));
    }

    @Test
    public void fullSubDomainUrlsAreUnchanged() {
        assertThat(testDomain.getSubDomainUrl("https://testdomain/subdomain/test"), equalTo("https://testdomain/subdomain/test"));
    }

    @Test
    public void otherDomainsAreJustReturned() {
        assertThat(testDomain.getSubDomainUrl("https://anothertestdomain/"), equalTo("https://anothertestdomain/"));
    }

}